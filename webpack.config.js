var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
//var merge = require('webpack-merge');

module.exports = {
	context: __dirname,
	entry: './js/app',
/*	{
		bundel: './js/app.js',
		styles: './css/style.css'
	},*/
	output: {
		path: __dirname + '/public',
		filename: './bundle.js',
        library: "mainApp"
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: "babel-loader",
				query: {
					presets : ['es2015']
					//optional: ['runtime']
				}
			},
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
			}
/*		{
			test: /\.css$/,
			loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
		},*/
			//{test: /\.js$/, loader: 'babel'}
		]
	},

	//watch: true,
	devtool: "source-map",

	plugins: [
		new ExtractTextPlugin('styles.css', {
			allChunks: true
		})
	]
};
