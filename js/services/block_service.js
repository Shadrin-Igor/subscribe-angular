angular.module('mainApp').factory( 'BlockService', function(  ){
  return {
    types : [
        {"id":1, "name":"Type 1", "type":"1-type", "params":{"class":"blockType1"}}, 
        {"id":2, "name":"Type 2", "type":"2-type", "params":{"class":"blockType2"}},
        {"id":3, "name":"Type 3", "type":"3-type", "params":{"class":"blockType3"}},
        {"id":4, "name":"Type 4", "type":"4-type", "params":{"class":"blockType4"}},
        {"id":5, "name":"Type 5", "type":"5-type", "params":{"class":"blockType5"}},
        {"id":6, "name":"Type 6", "type":"6-type", "params":{"class":"blockType6"}},
    ],
    sortBlock : function ( i, ii ){
      if( i.sort > ii.sort )return 1;
      if( i.sort < ii.sort )return -1;
      else return 0;
    },
    getBlockBy : function( list, id ) {
      for( var i=0;i<list.length;i++){
        if( list[i].id == id ){
          return i;
        }
      }
      return -1;
    }
  }
});