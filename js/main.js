var jquery = require("./lib/jquery.min.js");
var jqueryUI = require("./lib/jquery-ui.min.js");
var bootstrap = require("./lib/bootstrap.min.js");
var angular = require("./lib/angular.js");

//var ItemController =
var app = require("./app.js");
require("./controllers/LeftSectionController.js");
require("./controllers/RightSectionController.js");
require("./controllers/BlockController.js");