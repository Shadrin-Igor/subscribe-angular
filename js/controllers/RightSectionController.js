mainApp.controller('RightSectionController',
  function RightSectionController($scope, $http, $rootScope, $templateCache, $cacheFactory) {

      $scope.subjectParamsStatus = false;
      $scope.menu = [
          ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript','font-color', 'hilite-color', 'remove-format'],
          ['format-block'],
          ['font'],
          ['font-size'],
          ['ordered-list', 'unordered-list', 'outdent', 'indent'],
          ['left-justify', 'center-justify', 'right-justify'],
          ['code', 'quote', 'paragraph'],
          ['link', 'image'],
          ['css-class']
      ];
      
      $scope.uploadImage = function ( block, field, size ) {
        var size;
        if( !field )field= 'image';
        switch( size ){
            case 'medium' :size='260';break;
            case 'small' :size='160';break;
            default : size='580';break;
        }

        var url = 'http://info.embassyalliance.ru/upload/size/'+size;

        $('.fileupload').fileupload({
          url: url,
          dataType: 'json',
          done: function (e, data) {
              var file = data._response.result.files[0];
    
              if( !file.error ){
                  $scope.$apply(function () {
                      $scope.selectBlock.params[field] = file.url;
                      $('#progress .progress-bar').css('width','0%');
                      $rootScope.$emit('onSelectBlockSave');
                  });
              }
          },
          fail : function () {
              console.log('err');
          },
          progressall: function (e, data) {
              var progress = parseInt(data.loaded / data.total * 100, 10);
              $('#progress .progress-bar').css('width',progress + '%');
          }
        }).prop('disabled', !$.support.fileInput)
          .parent().addClass($.support.fileInput ? undefined : 'disabled');
      }
          
      $scope.textInput = function( block, name, field ) {
          return '<div class="form-group"><label for="text">'+name+':</label><textarea class="form-control" rows="5" id="text" ng-model="selectBlock.params.text" placeholder="Text"></textarea></div>';
      };

    $scope.deleteImage = function ( image ) {
    var imageParams = image.split( "/" );
    if( imageParams.length > 0 ){

        var fileName = imageParams[ imageParams.length - 1 ];
        //"http://info.embassyalliance.ru/upload/index.php?file=The-Orchard-Wellness--Health-Resort-Malaysia-photos-Exterior.JPEG"
        $http.delete('http://info.embassyalliance.ru/upload/index.php?file='+fileName)
            .success(function (data) {

                if( data[ fileName ] ){
                    $scope.selectBlock.params.image = '';
                    $('#progress .progress-bar').css('width','0%');
                    $rootScope.$emit('onSelectBlockSave');
                }
            });
    }
    };
      
    $rootScope.$on('onSelectBlock', function (){
        $scope.selectBlock = $rootScope.selectBlock;
    });

    $scope.save = function( block ) {
        $rootScope.$emit('onSelectBlockSave');
        $scope.selectBlock = {id:0};
        $scope.subjectParamsStatus = false;
    };

    $scope.cancel = function( ) {
        $scope.selectBlock = {id:0};
        console.log( $rootScope.selectBlock );
        $scope.subjectParamsStatus = false;
    };
  }
);