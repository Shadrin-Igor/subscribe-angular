mainApp.controller('ItemController',
    function ItemController($scope, $http, $rootScope, $window, $routeParams, BlockService) {
        $scope.centerData = {blocks:[]};
        $scope.saveDate = 0;
        $scope.openParams = false;

        // Отслеживаем прокрутку
        angular.element($window).bind("scroll", function() {
            var leftBlock = angular.element( document.getElementsByClassName("LeftSection")[0] );
            var rightBlock = angular.element( document.getElementsByClassName("RightSection")[0] );
            if (this.pageYOffset >= 100) {
                leftBlock.addClass('BPositionTop');
                rightBlock.addClass('BPositionTop');
                console.log('Scrolled below header.');
            } else {
                leftBlock.removeClass('BPositionTop');
                rightBlock.removeClass('BPositionTop');
                console.log('Header is in view.');
            }
        });

        // Загружаем данные с сервера
        if( $routeParams.id && $routeParams.id >0 ){
            $http.get('http://info.embassyalliance.ru/api/subscribe/id/'+$routeParams.id+'/'+( parseInt( Math.random() * 10000000 ) ) ).success(function (data) {
                $scope.centerData = data;
                if( $scope.centerData.params && !$scope.centerData.params.name )$scope.openParams = true;
                $('.droppable').sortable({
                    update:  function (event, ui) {

                        // Check order in HTML
                        var sortList={};
                        var sortPos = 0;
                        $('.droppable .CBItem, .droppable .ui-sortable-placeholder').map( function ( pos, value ){
                            if( !$(value).attr("id") && newId )$(value).attr( "id", 'block'+newId );
                            if( $(value).attr("id") ){
                                sortPos++;
                                sortList[ $(value).attr("id") ] = sortPos;
                            }
                        });

                        // Save order in centerData
                        $scope.centerData.blocks.map( function ( value ){
                            value.sort = ( sortList[ "block"+value.id ] ? sortList[ "block"+value.id ] : $scope.centerData.blocks.length+1 );

                        });

                        // Sort centerData
                        var list = $scope.centerData.blocks.sort( BlockService.sortBlock );

                        $rootScope.$emit('onSelectBlockSave');
                    }
                });
            })
                .error(function (data, status, headers, config) {
                    alert( "Have a http error" );
                });
        }
            else {
            $scope.openParams = true;
        }

        $('.droppable').droppable({
            drop: function( e, ui ) {
                var draggableElement = $(ui.draggable[0]);
                var newElement= {};
                var newId = '';

                if( draggableElement.attr("data-type") ) {
                    BlockService.types.map( function( value ){
                        if( value.type == draggableElement.attr("data-type") ){
                            newId = $scope.centerData.blocks.length + 1;
                            newElement = {id:newId, sort:1, type: value.type, params:{ subject: '', text:'' } };
                            $scope.centerData.blocks.push( newElement );
                            draggableElement.removeAttr( "data-type" );
                            draggableElement.attr("id", 'block'+newId);
                            draggableElement.remove();

                            // Check order in HTML
                            var sortList={};
                            var sortPos = 0;
                            $('.droppable .CBItem, .droppable .ui-sortable-placeholder').map( function ( pos, value ){
                                if( !$(value).attr("id") && newId )$(value).attr( "id", 'block'+newId );
                                if( $(value).attr("id") ){
                                    sortPos++;
                                    sortList[ $(value).attr("id") ]=sortPos;
                                }
                            });

                            // Save order in centerData
                            $scope.centerData.blocks.map( function ( value ){
                                value.sort = ( sortList[ "block"+value.id ] ? sortList[ "block"+value.id ] : $scope.centerData.blocks.length+1 );
                            });

                            // Sort centerData
                            var list = $scope.centerData.blocks.sort( BlockService.sortBlock );

                            $scope.$apply();
                        }
                    })
                }
            }
        });

        $scope.selectBlock = function ( block ){
            $rootScope.selectBlock = block;
            $rootScope.$emit('onSelectBlock');
        };

        $scope.blockDelete = function ( id ){
            var blocks = $scope.centerData.blocks;
            for( var i=0;i< blocks.length;i++){
              if( blocks[i].id == id ) {
                $scope.centerData.blocks.splice(i, 1);
                $("#block"+id).hide( 200 );
                break;
              }
            }
        };

        $scope.saveItem = function (){
            $scope.saveItemOnServer();
        };

        $scope.routeId = $routeParams.id;
        
        $rootScope.$on('onSelectBlockSave', function (){
            $scope.saveItemOnServer();
        });
        
        $scope.getBlockStructure = function( block ) {
            return BlockService.getBlockHTML( block );
        };

        $scope.filterDeleteItems = function( field ) {
            if( field.delete == true ) return false;
                else return true;
        }

        $scope.saveItemOnServer = function() {
            var jsonSave = JSON.stringify($scope.centerData);
            $http({contentType: "application/json",
                method: 'POST', url: 'http://info.embassyalliance.ru/api/save/',
                data:jsonSave })
                .success(function(data){
                    $scope.saveDate = new Date().getTime();
                    $scope.centerData = data;
                })
                .error(function(d){ console.log( "nope" ); });
        }
    }
);