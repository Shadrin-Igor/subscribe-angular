mainApp.controller('BlockController',
  function BlockController($scope, $http, $rootScope, $sce, BlockService) {

    $scope.textBlock = function( block, field ) {
        if( !field )field = 'text';
        return ( block.params[field] !== null && block.params[field] ) ? $scope.trustAsHtml( block.params[field] ) : '<i>Text</i>' ;
    };
      
    $scope.imageBlock = function( block, field ){
        if( !field )field = 'image';
        return ( $scope.block.params[field] !== null && $scope.block.params[field] && $scope.block.params[field] != 'del' ) ? '<img src="'+$scope.block.params[field]+'" />' : '<i>Image</i>' ;
    }; 
      
    $scope.subjectBlock = function( block ) {

        return ( block.params.subject != null && block.params.subject ) ? block.params.subject : '<i>Subject</i>';
    };

    $scope.subjectStyle = function ( block ) {
        var style = '';
        if( block.params.subjectparams ) {
            if( block.params.subjectparams.font )style += 'font-family: ' + block.params.subjectparams.font+';';
            if( block.params.subjectparams.size )style += 'font-size: ' + block.params.subjectparams.size+'px;';
            if( block.params.subjectparams.color )style += 'color: ' + block.params.subjectparams.color+';';
        }
        return style;
    }

  $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
  };

    switch( $scope.block.type ){
      case '3-type' :
          $scope.textCols = $scope.block.params.image !== null ? 9 : 12;
          $scope.imageCols = $scope.block.params.text !== null ? 2 : 12;
          break;
    }

    $scope.blockClose = function( id, field ){
        var itemIndex = BlockService.getBlockBy( $scope.centerData.blocks, id );
        if( itemIndex > -1 ){
            $scope.centerData.blocks[itemIndex].params[field] = 'del';
            var delBlock = $(".item-"+id).find(".block-"+field);
            delBlock.hide(200, function(){
                delBlock.remove();
                if( $(".item-"+id+" .blocks").length ==0 ){
                    $scope.centerData.blocks[itemIndex].delete = true;
                    $(".item-"+id).hide();
                }
            });
            $rootScope.$emit('onSelectBlockSave');
        }
    }

    $scope.itemClose = function( id ){
        var itemIndex = BlockService.getBlockBy( $scope.centerData.blocks, id );
        if( itemIndex > -1 ){
            $scope.centerData.blocks[itemIndex].delete = true;
            $(".item-"+id).hide( 200 );
            $rootScope.$emit('onSelectBlockSave');
        }
    }
  }
);