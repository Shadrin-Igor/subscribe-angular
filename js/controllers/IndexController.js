mainApp.controller('IndexController',
    function indexController($scope, $http, $window, $rootScope) {
        $scope.list = [];

        $http.get( "http://info.embassyalliance.ru/api/list/"+(parseInt( Math.random() * 100000 )) )
            .success(function (data) {
                if( data && data.length >0 )
                    $scope.list = data;
            });

        $scope.templateName = function( id ) {
            for( var i=0;i<$scope.templates.length;i++ ){
                if( id == $scope.templates[i].id )return $scope.templates[i].name;
            }
        };

        $scope.getDescription = function( id ){
            $window.location.href = "#/item/"+id;
        }

        $scope.addItem = function (){
            $window.location.href = "#/item/";
        }
    }
);