mainApp.controller('LeftSectionController',
  function leftSectionController($scope, $http, BlockService) {

      $scope.loadDraggable = function() {
          $('.draggable').draggable({
              connectToSortable: '.droppable',
              helper: "clone"

          });
      }

      $scope.listTypes = BlockService.types;
  }
);