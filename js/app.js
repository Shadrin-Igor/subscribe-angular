var mainApp = angular.module('mainApp', ["ngRoute", "ngSanitize", 'colorpicker.module', 'wysiwyg.module'])
    .config(function($routeProvider){
        $routeProvider.when('/item/:id',
        {
            templateUrl:'views/item.html',
            controller:'ItemController'
        })
        .when('/item/',
            {
                templateUrl:'views/item.html',
                controller:'ItemController'
            })
        .when('/preview/:id',
            {
                templateUrl:'views/preview.html',
                controller:'PreviewController'
            })
        .when('/index',
        {
              templateUrl:'views/index.html',
              controller:'IndexController'
        });
        $routeProvider.otherwise({redirectTo: '/index'});
    })
    .run( function( $rootScope, $http ){
        $rootScope.templates = [];
        $http.get( "http://info.embassyalliance.ru/api/template" )
            .success(function (data) {
                if( data && data.length >0 )
                    $rootScope.templates = data;
            });
    });


